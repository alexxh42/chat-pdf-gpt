import { EntityResponse } from '../dto/entity-response.dto';

export function queryDto(dto: EntityResponse): string {
  const selectedFields = Object.keys(dto);
  return selectedFields.join(' ');
}
