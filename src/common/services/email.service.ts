import { Injectable } from '@nestjs/common';
import * as nodemailer from 'nodemailer';

@Injectable()
export class EmailService {
  private transporter: nodemailer.Transporter;

  constructor() {
    this.transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: Number(process.env.SMTP_PORT),
      secure: process.env.SMTP_SECURE === 'true',
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASS,
      },
    });
  }

  async sendEmail(data: { to: string; subject: string; html: string }) {
    try {
      const info = await this.transporter.sendMail({
        from: process.env.SMTP_FROM,
        to: data.to,
        subject: data.subject,
        html: data.html,
      });
      console.log(`Email enviado: ${info.messageId}`);
    } catch (error) {
      console.error(`Error al enviar el email: ${error}`);
      throw error;
    }
  }
}
