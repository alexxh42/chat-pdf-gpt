import { Injectable } from '@nestjs/common';
import * as qrcode from 'qrcode';
import * as fs from 'fs';

@Injectable()
export class QrCodeService {
  async generateQrCodeBase64(str: string): Promise<string> {
    const qrCodeDataUri = await qrcode.toDataURL(str);
    const data = qrCodeDataUri.replace(/^data:image\/\w+;base64,/, '');
    const buffer = Buffer.from(data, 'base64');
    const base64 = buffer.toString('base64');
    return base64;
  }
}
