import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

export enum MessageRole {
  SYSTEM = 'system',
  ASSISTANT = 'assistant',
  USER = 'user',
}

export class OpenAiMessage {
  role: MessageRole | string;
  content: string;
  date?: Date;
}
@Injectable()
export class OpenAIService {
  private messages: Array<OpenAiMessage> = [];
  constructor(private httpService: HttpService) {}

  loadSystem(content: string) {
    this.messages.push({ role: MessageRole.SYSTEM, content });
    return this;
  }

  loadAssistant(content: string) {
    this.messages.push({ role: MessageRole.ASSISTANT, content });
    return this;
  }

  loadUser(content: string) {
    this.messages.push({ role: MessageRole.USER, content });
    return this;
  }

  load(messages: Array<OpenAiMessage>) {
    messages.map(({ role, content }: OpenAiMessage) =>
      this.messages.push({ role, content }),
    );
    return this;
  }

  async generate(): Promise<string> {
    const response = await this.httpService
      .post(
        'https://api.openai.com/v1/chat/completions',
        {
          model: process.env.OPENAI_MODEL,
          messages: this.messages,
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${process.env.OPENAI_API_KEY}`,
          },
        },
      )
      .toPromise();
    return response.data.choices[0].message.content;
  }
}
