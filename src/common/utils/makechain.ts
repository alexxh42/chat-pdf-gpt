import { ChatOpenAI } from 'langchain/chat_models/openai';
import { PineconeStore } from 'langchain/vectorstores/pinecone';
import { ConversationalRetrievalQAChain } from 'langchain/chains';

const CONDENSE_TEMPLATE = `Dada la siguiente conversación y una pregunta de seguimiento, reformula la pregunta de seguimiento para que sea una pregunta independiente.

Historial del chat:
{chat_history}
Pregunta de seguimiento: {question}
Pregunta independiente:`;

const QA_TEMPLATE = `Eres un asistente AI servicial. Utiliza los siguientes fragmentos de contexto para responder la pregunta al final.
Si no conoces la respuesta, simplemente di que no lo sabes. NO intentes inventar una respuesta.
Si la pregunta no está relacionada con el contexto, responde amablemente que estás configurado para responder solo preguntas relacionadas con el contexto.

{context}

Pregunta: {question}
Respuesta útil en formato markdown:`;

export const makeChain = (vectorstore: PineconeStore, modelName) => {
  const model = new ChatOpenAI({
    temperature: 1, // increase temepreature to get more creative answers
    modelName, //change this to gpt-4 if you have access
  });

  const chain = ConversationalRetrievalQAChain.fromLLM(
    model,
    vectorstore.asRetriever(),
    {
      qaTemplate: QA_TEMPLATE,
      questionGeneratorTemplate: CONDENSE_TEMPLATE,
      returnSourceDocuments: true, //The number of source documents returned is 4 by default
    },
  );
  return chain;
};
