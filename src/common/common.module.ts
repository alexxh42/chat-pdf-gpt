import { Module } from '@nestjs/common';
import { QrCodeService } from './services/qr.service';
import { EmailService } from './services/email.service';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { OpenAIService } from './services/open-ai.service';
import { DocumentsService } from '../documents/documents.service';
import { HttpModule } from '@nestjs/axios';
import { DocumentsModule } from 'src/documents/documents.module';
import { MongooseModule } from '@nestjs/mongoose';
import {
  DocumentFile,
  DocumentFileSchema,
} from 'src/documents/entities/document-file.entity';

@Module({
  imports: [
    ConfigModule,
    MongooseModule.forFeature([
      { name: DocumentFile.name, schema: DocumentFileSchema },
    ]),
    JwtModule.register({
      secret: process.env.JWT_SECRET_KEY,
      signOptions: { expiresIn: 3600 },
    }),
    DocumentsModule,
    HttpModule,
  ],
  controllers: [],
  providers: [QrCodeService, EmailService, DocumentsService, OpenAIService],
  exports: [QrCodeService, EmailService, DocumentsService, OpenAIService],
})
export class CommonModule {}
