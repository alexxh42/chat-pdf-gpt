import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  S3Client,
  PutObjectCommand,
  DeleteObjectCommand,
} from '@aws-sdk/client-s3';
import { AskDocumentDto } from './dto/ask-document.dto';
import { InjectModel } from '@nestjs/mongoose';
import {
  DocumentFile,
  DocumentFileDocument,
} from './entities/document-file.entity';
import { Model } from 'mongoose';

import { RecursiveCharacterTextSplitter } from 'langchain/text_splitter';
import { OpenAIEmbeddings } from 'langchain/embeddings/openai';
import { PineconeStore } from 'langchain/vectorstores/pinecone';
import { PDFLoader } from 'langchain/document_loaders/fs/pdf';
import { makeChain } from '../common/utils/makechain';
import { AIMessage, HumanMessage } from 'langchain/schema';
import { PineconeClient } from '@pinecone-database/pinecone';

@Injectable()
export class DocumentsService {
  constructor(
    @InjectModel(DocumentFile.name)
    private documentFileModel: Model<DocumentFileDocument>,
    private configService: ConfigService,
  ) {}

  bucketName = this.configService.get('AWS_S3_BUCKET_NAME');
  s3 = new S3Client({
    region: process.env.AWS_REGION,
  });

  //application/pdf
  async uploadPublicFile(
    dataBuffer: Buffer,
    filename: string,
    ContentType?: string,
    namespace?: string,
  ) {
    if (ContentType != 'application/pdf')
      throw new ConflictException('document must be application/pdf');

    try {
      const Key = `documents/${encodeURIComponent(
        Date.now().toString() + '-' + filename.replace(/[^a-zA-Z0-9]/g, '_'),
      )}`;
      const file = {
        Bucket: this.bucketName,
        Body: dataBuffer,
        Key,
        ACL: 'public-read',
        //Endpoint: `${this.bucketName}.${process.env.AWS_REGION}.s3.amazonaws.com`,
        ContentDisposition: 'inline',
      };
      if (ContentType) file['ContentType'] = ContentType;

      const documentResult = await this.s3.send(new PutObjectCommand(file));
      const fileUrl = `https://${this.bucketName}.s3.amazonaws.com/${Key}`;

      // Crear el usuario y guardarlo en la base de datos
      const newDocumentFile = new this.documentFileModel({
        namespace,
        key: Key,
        upload_result: documentResult,
        url: fileUrl,
        created_at: new Date(),
        updated_at: new Date(),
      });
      const savedDocumentFile = await newDocumentFile.save();
      const dataBlob = new Blob([dataBuffer], { type: ContentType });
      await this.ingest(namespace, dataBlob);

      return savedDocumentFile;
    } catch (error) {
      throw new ConflictException(error);
    }
  }

  async ingest(namespace, documentBuffer: Blob) {
    try {
      const pinecone = new PineconeClient();

      await pinecone.init({
        environment: process.env.PINECONE_ENVIRONMENT ?? '', //this is in the dashboard
        apiKey: process.env.PINECONE_API_KEY ?? '',
      });
      const documentBlob = new Blob([documentBuffer]);
      const documentLoader = new PDFLoader(documentBlob);
      const rawDocs = await documentLoader.load();

      /* Split text into chunks */
      const textSplitter = new RecursiveCharacterTextSplitter({
        chunkSize: 1000,
        chunkOverlap: 200,
      });

      const docs = await textSplitter.splitDocuments(rawDocs);
      console.log('split docs', docs);

      console.log('creating vector store...');
      /*create and store the embeddings in the vectorStore*/
      const embeddings = new OpenAIEmbeddings();

      const index = pinecone.Index(process.env.PINECONE_INDEX_NAME); //change to your own index name

      //embed the PDF documents
      await PineconeStore.fromDocuments(docs, embeddings, {
        pineconeIndex: index,
        namespace: '', //namespace,
        textKey: 'text',
      });
    } catch (error) {
      console.log('error', error);
      throw new Error('Failed to ingest your data');
    }
  }

  async deletePublicFile(id: string) {
    const foundDocumentFile = await this.documentFileModel.findById(id).exec();
    if (!foundDocumentFile) throw new NotFoundException('Document not found');

    try {
      const deletedFile = await this.s3.send(
        new DeleteObjectCommand({
          Bucket: this.bucketName,
          Key: foundDocumentFile.key,
        }),
      );

      foundDocumentFile.is_deleted = true;
      foundDocumentFile.is_active = false;
      foundDocumentFile.save();

      return deletedFile;
    } catch (error) {
      console.log(error);
    }
  }

  async askNamespace(namespace: string, askDocumentDto: AskDocumentDto) {
    const pinecone = new PineconeClient();

    await pinecone.init({
      environment: process.env.PINECONE_ENVIRONMENT ?? '', //this is in the dashboard
      apiKey: process.env.PINECONE_API_KEY ?? '',
    });

    const { question, history } = askDocumentDto;

    if (!namespace) throw new ConflictException('No namespace in the request');
    if (!question) throw new ConflictException('No question in the request');

    const sanitizedQuestion = question.trim().replaceAll('\n', ' ');

    try {
      const index = pinecone.Index(process.env.PINECONE_INDEX_NAME);

      /* create vectorstore*/
      const vectorStore = await PineconeStore.fromExistingIndex(
        new OpenAIEmbeddings({}),
        {
          pineconeIndex: index,
          textKey: 'text',
          namespace: '', //namespace, //<-- namespace function parameters
        },
      );

      //create chain
      const chain = makeChain(vectorStore, process.env.OPENAI_MODEL ?? 'gpt-4');

      const pastMessages = history.map((message: string, i: number) => {
        if (i % 2 === 0) {
          return new HumanMessage(message);
        } else {
          return new AIMessage(message);
        }
      });

      //Ask a question using chat history
      const response = await chain.call({
        question: sanitizedQuestion,
        chat_history: pastMessages,
      });

      console.log('response', response);
      return response;
    } catch (error: any) {
      console.log('error', error);
      throw new ConflictException('Something went wrong');
    }
  }
}
