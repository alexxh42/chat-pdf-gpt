import { CoreEntity } from '../../common/entities/core.entity';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

export type DocumentFileDocument = DocumentFile & Document;

@Schema({ collection: 'document' })
export class DocumentFile extends CoreEntity {
  @ApiProperty()
  @Prop({ required: true, unique: true })
  key: string;

  @ApiProperty()
  @Prop({ type: Object })
  document_result: any;

  @ApiProperty()
  @Prop({ required: true, unique: true })
  url: string;

  @ApiProperty()
  @Prop()
  namespace: string;

  @ApiProperty()
  @Prop()
  extract: string;

  @ApiProperty()
  @Prop({ default: true })
  is_active?: boolean = true;

  @ApiProperty()
  @Prop()
  is_deleted?: boolean;
}

export const DocumentFileSchema = SchemaFactory.createForClass(DocumentFile);
