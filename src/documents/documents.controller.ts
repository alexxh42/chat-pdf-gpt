import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';

import { DocumentsService } from './documents.service';
import { AskDocumentDto } from './dto/ask-document.dto';

@ApiTags('Documents')
@Controller('documents')
export class DocumentsController {
  constructor(private documentService: DocumentsService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async documentFile(
    @Body('namespace') documentNamespace: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    const documentFile = await this.documentService.uploadPublicFile(
      file.buffer,
      file.originalname,
      file.mimetype,
      documentNamespace,
    );

    return documentFile;
  }

  // @Get('')
  // async getAllNamespaces() {
  //   return this.documentService.getAllNamespaces();
  // }

  // @Get('/:namespace')
  // async getNamespace(@Param('namespace') namespace: string) {
  //   return this.documentService.getNamespace(namespace);
  // }

  @Post('/:namespace')
  async askFile(
    @Param('namespace') namespace: string,
    @Body() askDocumentDto: AskDocumentDto,
  ) {
    return this.documentService.askNamespace(namespace, askDocumentDto);
  }

  // @Delete('/:key')
  // async deleteFile(@Param('key') fileKey: string) {
  //   return this.documentService.deletePublicFile(fileKey);
  // }
}
