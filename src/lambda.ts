// lambda.ts
import { Handler, Context } from 'aws-lambda';
import { Server } from 'http';
import { createServer, proxy } from 'aws-serverless-express';
import { eventContext } from 'aws-serverless-express/middleware';
import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import * as bodyParser from 'body-parser';
import express from 'express';

// NOTE: If you get ERR_CONTENT_DECODING_FAILED in your browser, this is likely
// due to a compressed response (e.g. gzip) which has not been handled correctly
// by aws-serverless-express and/or API Gateway. Add the necessary MIME types to
// binaryMimeTypes below
const binaryMimeTypes: string[] = [];

let cachedServer: Server;

async function bootstrapServer(): Promise<Server> {
  if (!cachedServer) {
    const expressApp = express();
    const nestApp = await NestFactory.create(
      AppModule,
      new ExpressAdapter(expressApp),
      { cors: { origin: '*' } },
    );
    nestApp.enableCors();
    nestApp.setGlobalPrefix('api');
    nestApp.useGlobalPipes(new ValidationPipe());
    /*
      const config = new DocumentBuilder()
         .setTitle('Nuclear')
         .setDescription('Nuclear API')
         .setVersion('1.0')
         .addTag('saas')
         .build();
      const document = SwaggerModule.createDocument(nestApp, config);
      SwaggerModule.setup('docs', nestApp, document);
      */
    nestApp.use(eventContext());
    nestApp.use(bodyParser.json({ limit: '50mb' }));
    nestApp.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    await nestApp.init();
    cachedServer = createServer(expressApp, undefined, binaryMimeTypes);
  }
  return cachedServer;
}

export const handler: Handler = async (event: any, context: Context) => {
  cachedServer = await bootstrapServer();
  return proxy(cachedServer, event, context, 'PROMISE').promise;
};
