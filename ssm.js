// ssm.js - Parameters Store / Environment Variables from AWS
// Reference: https://dev.to/davidshaek/how-to-save-up-on-aws-parameter-store-with-serverless-framework-44mm
const SSM = require('aws-sdk/clients/ssm');

module.exports.getParameters = async ({ resolveConfigurationProperty }) => {
  const region = await resolveConfigurationProperty(['provider', 'region']);
  const ssm = new SSM({ region });

  const policy = await ssm
    .getParameter({
      // PARAMETER STORE NAME
      Name: 'DOCUMENT_GPT_ENV_VARS',
      WithDecryption: true,
    })
    .promise();
  return JSON.parse(policy.Parameter.Value);
};
